#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include "worldcat.h"
#include "isbn.h"
#include "addbookdialog.h"
#include <qobject.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    // Isbn isbn_nr(QString("0306406152"));
    //qDebug()<<isbn_nr.getIsbn();

   // book.search(isbn_nr.getIsbn());

    w.show();

    return a.exec();
}
