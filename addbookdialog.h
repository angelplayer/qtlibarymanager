#ifndef ADDBOOKDIALOG_H
#define ADDBOOKDIALOG_H

#include <QtCore/QDebug>
#include "worldcat.h"
#include "book.h"
#include <QtWidgets/QDialog>

namespace Ui {
class addBookDialog;
}

class addBookDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addBookDialog(QWidget *parent = 0);
    ~addBookDialog();
public slots:
    void setValues(book*);

private slots:
    void on_buttonBox_accepted();
signals:
    void finished();
private:
    Ui::addBookDialog *ui;
    book *bookTemp;
};

#endif // ADDBOOKDIALOG_H
