#include "book.h"
#include <qdebug.h>

book::book(){
}


void book::setIsbn(QString isbn){
    this->isbn = isbn;
}
void book::setAuthor(QString author){
    this->author = author;
}
void book::setTitle(QString title){
    this->title = title;
}
void book::setEdition(QString edition){
    this->edition = edition;
}
void book::setPublisher(QString publisher){
    this->publisher = publisher;
}
void book::setYear(QString year){
    this->year = year;
}
void book::setCity(QString city){
    qDebug()<<city;
    this->city = city;
}
void book::setLanguage(QString language){
    this->language = language;
}

QString book::getIsbn() const{
    return isbn;
}
QString book::getAuthor() const{
    return author;
}
QString book::getTitle() const{
    return title;
}
QString book::getEdition() const{
    return edition;
}
QString book::getPublisher() const{
    return publisher;
}
QString book::getYear() const{
    return year;
}
QString book::getCity() const{
    return city;
}
QString book::getLanguage() const{
    return language;
}
