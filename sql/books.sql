--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authors; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE authors (
    id integer NOT NULL,
    firstname character varying,
    lastname character varying
);


ALTER TABLE authors OWNER TO books;

--
-- Name: bibs; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE bibs (
    id integer NOT NULL,
    city character varying,
    street character varying,
    room character varying,
    country character varying
);


ALTER TABLE bibs OWNER TO books;

--
-- Name: books; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE books (
    isbn13 integer,
    year integer,
    form integer,
    author integer,
    title character varying,
    publisher character varying,
    city character varying,
    edition character varying,
    oclc character varying,
    id integer NOT NULL,
    inventory_id integer
);


ALTER TABLE books OWNER TO books;

--
-- Name: customers; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE customers (
    matrikelnr integer,
    id integer NOT NULL,
    firstname character varying,
    lastname character varying,
    telnr integer,
    city character varying,
    adress character varying,
    email character varying
);


ALTER TABLE customers OWNER TO books;

--
-- Name: inventory; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE inventory (
    index character varying NOT NULL,
    bib integer,
    status integer,
    id integer NOT NULL
);


ALTER TABLE inventory OWNER TO books;

--
-- Name: inventory_status; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE inventory_status (
    id integer NOT NULL,
    title character varying
);


ALTER TABLE inventory_status OWNER TO books;

--
-- Name: lend; Type: TABLE; Schema: public; Owner: books; Tablespace: 
--

CREATE TABLE lend (
    id integer NOT NULL,
    id_customer integer,
    date_lend date,
    date_due date,
    id_inventory integer
);


ALTER TABLE lend OWNER TO books;

--
-- Data for Name: authors; Type: TABLE DATA; Schema: public; Owner: books
--

COPY authors (id, firstname, lastname) FROM stdin;
\.


--
-- Data for Name: bibs; Type: TABLE DATA; Schema: public; Owner: books
--

COPY bibs (id, city, street, room, country) FROM stdin;
\.


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: books
--

COPY books (isbn13, year, form, author, title, publisher, city, edition, oclc, id, inventory_id) FROM stdin;
\.


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: books
--

COPY customers (matrikelnr, id, firstname, lastname, telnr, city, adress, email) FROM stdin;
\.


--
-- Data for Name: inventory; Type: TABLE DATA; Schema: public; Owner: books
--

COPY inventory (index, bib, status, id) FROM stdin;
\.


--
-- Data for Name: inventory_status; Type: TABLE DATA; Schema: public; Owner: books
--

COPY inventory_status (id, title) FROM stdin;
\.


--
-- Data for Name: lend; Type: TABLE DATA; Schema: public; Owner: books
--

COPY lend (id, id_customer, date_lend, date_due, id_inventory) FROM stdin;
\.


--
-- Name: bibs_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY bibs
    ADD CONSTRAINT bibs_pkey PRIMARY KEY (id);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY authors
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- Name: inventory_index_key; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY inventory
    ADD CONSTRAINT inventory_index_key UNIQUE (index);


--
-- Name: inventory_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (id);


--
-- Name: inventory_status_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY inventory_status
    ADD CONSTRAINT inventory_status_pkey PRIMARY KEY (id);


--
-- Name: lend_pkey; Type: CONSTRAINT; Schema: public; Owner: books; Tablespace: 
--

ALTER TABLE ONLY lend
    ADD CONSTRAINT lend_pkey PRIMARY KEY (id);


--
-- Name: authors_firstname_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX authors_firstname_idx ON authors USING btree (firstname);


--
-- Name: authors_lastname_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX authors_lastname_idx ON authors USING btree (lastname);


--
-- Name: books_author_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX books_author_idx ON books USING btree (author);


--
-- Name: books_inventory_id_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX books_inventory_id_idx ON books USING btree (inventory_id);


--
-- Name: books_isbn13_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX books_isbn13_idx ON books USING btree (isbn13);


--
-- Name: inventory_bib_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX inventory_bib_idx ON inventory USING btree (bib);


--
-- Name: inventory_index_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX inventory_index_idx ON inventory USING btree (index);


--
-- Name: inventory_index_idx1; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX inventory_index_idx1 ON inventory USING btree (index);


--
-- Name: inventory_status_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX inventory_status_idx ON inventory USING btree (status);


--
-- Name: lend_id_customer_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX lend_id_customer_idx ON lend USING btree (id_customer);


--
-- Name: lend_id_inventory_idx; Type: INDEX; Schema: public; Owner: books; Tablespace: 
--

CREATE INDEX lend_id_inventory_idx ON lend USING btree (id_inventory);


--
-- Name: books_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_author_fkey FOREIGN KEY (author) REFERENCES authors(id);


--
-- Name: books_inventory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_inventory_id_fkey FOREIGN KEY (inventory_id) REFERENCES inventory(id);


--
-- Name: inventory_bib_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY inventory
    ADD CONSTRAINT inventory_bib_fkey FOREIGN KEY (bib) REFERENCES bibs(id);


--
-- Name: inventory_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY inventory
    ADD CONSTRAINT inventory_status_fkey FOREIGN KEY (status) REFERENCES inventory_status(id);


--
-- Name: lend_id_customer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY lend
    ADD CONSTRAINT lend_id_customer_fkey FOREIGN KEY (id_customer) REFERENCES customers(id);


--
-- Name: lend_id_inventory_fkey; Type: FK CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY lend
    ADD CONSTRAINT lend_id_inventory_fkey FOREIGN KEY (id_inventory) REFERENCES inventory(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

