#ifndef INVENTORY_OBJECT_H
#define INVENTORY_OBJECT_H
#include <QSqlRecord>

class inventory_object
{
public:
    inventory_object();
    static inventory_object fromRow(QSqlRecord);
};

#endif // INVENTORY_OBJECT_H
