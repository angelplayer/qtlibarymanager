#ifndef WORLDCAT_H
#define WORLDCAT_H
#include <QtCore>
#include <QNetworkAccessManager>
#include <QObject>
#include <QNetworkReply>
#include <book.h>

class Worldcat : public QObject
{
    Q_OBJECT

public:
    Worldcat();
    Worldcat(book *book1, QString isbn);
    ~Worldcat();
    void search(QString);
    book *getBook();
    QString getIsbn();
    QString getAuthor();
    QString getTitle();
    QString getEdition();
    QString getPublisher();
    QString getYear();
    QString getCity();
    QString getLanguage();
private:
    book *book1;
    QString apiUri;
    QString isbn;
    QString author;
    QString title;
    QString edition;
    QString publisher;
    QString year;
    QString city;
    QString language;
    QNetworkAccessManager *networkManager;
    void setIsbn(QString isbn);
public:
    void setBook(book *book1);
    void setAuthor(QString author);
    void setTitle(QString title);
    void setEdition(QString edition);
    void setPublisher(QString publisher);
    void setYear(QString year);
    void setCity(QString city);
    void setLanguage(QString language);

signals:
    void worldcatFinished();

public slots:
    void replyFinished(QNetworkReply*networkReply);
};

#endif // WORLDCAT_H
