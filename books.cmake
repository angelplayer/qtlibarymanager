#-------------------------------------------------
#
# Project created by QtCreator 2015-03-29T16:56:56
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = books
TEMPLATE = app


SOURCES += main.cpp\
    isbn.cpp \
    worldcat.cpp \
    mainwindow.cpp \
    addbookdialog.cpp \
    book.cpp \
    booklist.cpp

HEADERS  += mainwindow.h \
    isbn.h \
    worldcat.h \
    addbookdialog.h \
    book.h \
    booklist.h

FORMS    += \
    mainwindow.ui \
    addbookdialog.ui
