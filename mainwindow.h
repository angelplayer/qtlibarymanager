#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "worldcat.h"
#include "addbookdialog.h"
#include "book.h"
#include "booklist.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void rebuildList();



signals:
    void addIsbn(Worldcat *);

private:
    BookList bookList;
    book *bookTemp = NULL;
    Ui::MainWindow *ui;
    Worldcat *worldcatLookUp = NULL;
    addBookDialog *dialog1 = NULL;
    void removeEntry(int id);

private slots:
    void on_addIsbn_clicked();
    void callDialog();
    void addEntry();
    void openFile();
    void saveFile();
    void on_lineEdit_textChanged(const QString &arg1);
};

#endif // MAINWINDOW_H
