#include "booklist.h"
#include "book.h"
#include <qdebug.h>

BookList::BookList(){
}

BookList::~BookList(){

}

void BookList::add(book newBook){
    bookList.append(newBook);
    updateBookListFiltered();
}

void BookList::removeBook(int id){
    bookList.removeAt(id);

}

void BookList::clearFilter(){
    this->searchTerm = "";
    this->column = 0;
    updateBookListFiltered();
}

void BookList::setFilter(const QString &searchTerm, int column){
    this->searchTerm = searchTerm;
    this->column = column;
    updateBookListFiltered();
}

void BookList::updateBookListFiltered(){
    bookListFiltered.clear();
    qDebug()<<bookList.length();
    for(int i=0;i<bookList.length();i++){
        if(searchTerm!=NULL){
            switch (this->column){
                case 2:
                    if(bookList[i].getTitle().contains(searchTerm,Qt::CaseInsensitive)){
                        bookListFiltered.append(&bookList[i]);
                    }
                    break;
            }
        }else{
            bookListFiltered.append(&bookList[i]);
        }
    }
}

QList<book*> BookList::getAllBooks(){
    QList<book*> books;
    for(int i=0;i<bookList.length();i++){
        books.append(&bookList[i]);
    }
    return books;
}

QList<book*> BookList::getBooks(){
    return bookListFiltered;
}

QList<book*> BookList::searchBooks(QString &searchTerm, int column){
    setFilter(searchTerm,column);
    return bookListFiltered;
}

int BookList::length(){
    return bookListFiltered.length();
}

book *BookList::at(int id){
    if(bookListFiltered.length()==0){
        return NULL;
    }
    return bookListFiltered.at(id);
}
