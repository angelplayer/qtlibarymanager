#include "addbookdialog.h"
#include "ui_addbookdialog.h"
#include "worldcat.h"
#include "book.h"

addBookDialog::addBookDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addBookDialog)
{
    ui->setupUi(this);
}

addBookDialog::~addBookDialog()
{
    delete ui;
}

void addBookDialog::setValues(book *book1){
    this->bookTemp = book1;
    ui->iSBNLineEdit->setText(book1->getIsbn());
    ui->authorLineEdit->setText(book1->getAuthor());
    ui->cityLineEdit->setText(book1->getCity());
    ui->publisherLineEdit->setText(book1->getPublisher());
    ui->yearLineEdit->setText(book1->getYear());
    ui->titleLineEdit->setText(book1->getTitle());
    ui->volumeLineEdit->setText(book1->getEdition());
}


void addBookDialog::on_buttonBox_accepted()
{
    bookTemp->setTitle(ui->titleLineEdit->text());
    bookTemp->setAuthor(ui->authorLineEdit->text());
    bookTemp->setEdition(ui->volumeLineEdit->text());
    bookTemp->setPublisher(ui->publisherLineEdit->text());
    bookTemp->setCity(ui->cityLineEdit->text());
    bookTemp->setYear(ui->yearLineEdit->text());
    emit finished();
}
