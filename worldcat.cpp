#include "worldcat.h"
#include <QtCore>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include "isbn.h"
#include "book.h"


Worldcat::Worldcat(book *book1, QString isbn)
{
    apiUri = "http://xisbn.worldcat.org/webservices/xid/isbn/";
    this->book1 = book1;
    search(isbn);
}

Worldcat::Worldcat()
{
    apiUri = "http://xisbn.worldcat.org/webservices/xid/isbn/";
}
Worldcat::~Worldcat(){
    //delete networkManager;
}

void Worldcat::search(QString searchTerm){
    if(book1!=NULL){
        setIsbn(searchTerm);
        QNetworkRequest request(apiUri+getIsbn()+QString("?method=getMetadata&format=json&fl=*"));
        networkManager = new QNetworkAccessManager(this);
        connect(networkManager,SIGNAL(finished(QNetworkReply*)),
                this,SLOT(replyFinished(QNetworkReply*)));
        networkManager->get(request);
    }
}

void Worldcat::replyFinished(QNetworkReply *networkReply){
    QByteArray byteResponse = networkReply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(byteResponse);
    QJsonObject jsonObj = jsonResponse.object();
    if(jsonObj["stat"]=="ok"){
        QJsonArray jsonArray = jsonObj["list"].toArray();
        QJsonObject jsonObj2 = jsonArray[0].toObject();
        setAuthor(jsonObj2["author"].toString());
        setTitle(jsonObj2["title"].toString());
        setEdition(jsonObj2["ed"].toString());
        setPublisher(jsonObj2["publisher"].toString());
        setYear(jsonObj2["year"].toString());
        setCity(jsonObj2["city"].toString());
        setLanguage(jsonObj2["lang"].toString());
    }
    emit worldcatFinished();
   // deleteLater();
}

//getter and setter...

void Worldcat::setBook(book* book1){
    this->book1 = book1;
}

void Worldcat::setIsbn(QString isbn){
    book1->setIsbn(isbn);
}
void Worldcat::setAuthor(QString author){
    book1->setAuthor(author);
}
void Worldcat::setTitle(QString title){
    book1->setTitle(title);
}
void Worldcat::setEdition(QString edition){
    book1->setEdition(edition);
}
void Worldcat::setPublisher(QString publisher){
    book1->setPublisher(publisher);
}
void Worldcat::setYear(QString year){
    book1->setYear(year);
}
void Worldcat::setCity(QString city){
    qDebug()<<city;
    book1->setCity(city);
}
void Worldcat::setLanguage(QString language){
    book1->setLanguage(language);
}

book *Worldcat::getBook(){
    return book1;
}

QString Worldcat::getIsbn(){
    return book1->getIsbn();
}
QString Worldcat::getAuthor(){
    return author;
}
QString Worldcat::getTitle(){
    return title;
}
QString Worldcat::getEdition(){
    return edition;
}
QString Worldcat::getPublisher(){
    return publisher;
}
QString Worldcat::getYear(){
    return year;
}
QString Worldcat::getCity(){
    return city;
}
QString Worldcat::getLanguage(){
    return language;
}
