#ifndef INVENTORY_H
#define INVENTORY_H
#include <QObject>
#include <QSql>
#include <QSqlDatabase>
#include <QVariant>
#include <vector>
template <typename T>
class inventory
{
protected:
    QSqlDatabase database;
public:
    inventory(QSqlDatabase);
    T getInventoryObjectById(int);
    T getInventoryObjectByName(QString);
    T getInventoryObjectByEAN13(QString);
    std::vector<T> getListOfLendObjects(QString);

};

#endif // INVENTORY_H
