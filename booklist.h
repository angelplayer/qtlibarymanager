#ifndef BOOKLIST_H
#define BOOKLIST_H

#include <QObject>
#include "book.h"

class BookList
{

public:
    explicit BookList();
    ~BookList();
    void setFilter(const QString &searchTerm,int column);
    void clearFilter();
    void add(book);
    void removeBook(int id);
    int length();
    QList<book*> getBooks();
    QList<book*> searchBooks(QString &searchTerm,int column);
    QList<book*> getAllBooks();
    book *at(int id);


private:
    QString searchTerm;
    int column;
    QList<book> bookList;
    QList<book*> bookListFiltered;
    void updateBookListFiltered();
};

#endif // BOOKLIST_H
