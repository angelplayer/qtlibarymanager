/********************************************************************************
** Form generated from reading UI file 'addbookdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDBOOKDIALOG_H
#define UI_ADDBOOKDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_addBookDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *iSBNLabel;
    QLineEdit *iSBNLineEdit;
    QLabel *titleLabel;
    QLineEdit *titleLineEdit;
    QLabel *authorLabel;
    QLineEdit *authorLineEdit;
    QLabel *volumeLabel;
    QLineEdit *volumeLineEdit;
    QLabel *cityLabel;
    QLineEdit *cityLineEdit;
    QLabel *publisherLabel;
    QLineEdit *publisherLineEdit;
    QLabel *yearLabel;
    QLineEdit *yearLineEdit;

    void setupUi(QDialog *addBookDialog)
    {
        if (addBookDialog->objectName().isEmpty())
            addBookDialog->setObjectName(QString::fromUtf8("addBookDialog"));
        addBookDialog->resize(503, 446);
        buttonBox = new QDialogButtonBox(addBookDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(140, 400, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(addBookDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(29, 29, 461, 331));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        iSBNLabel = new QLabel(formLayoutWidget);
        iSBNLabel->setObjectName(QString::fromUtf8("iSBNLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, iSBNLabel);

        iSBNLineEdit = new QLineEdit(formLayoutWidget);
        iSBNLineEdit->setObjectName(QString::fromUtf8("iSBNLineEdit"));
        iSBNLineEdit->setReadOnly(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, iSBNLineEdit);

        titleLabel = new QLabel(formLayoutWidget);
        titleLabel->setObjectName(QString::fromUtf8("titleLabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, titleLabel);

        titleLineEdit = new QLineEdit(formLayoutWidget);
        titleLineEdit->setObjectName(QString::fromUtf8("titleLineEdit"));

        formLayout->setWidget(1, QFormLayout::FieldRole, titleLineEdit);

        authorLabel = new QLabel(formLayoutWidget);
        authorLabel->setObjectName(QString::fromUtf8("authorLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, authorLabel);

        authorLineEdit = new QLineEdit(formLayoutWidget);
        authorLineEdit->setObjectName(QString::fromUtf8("authorLineEdit"));

        formLayout->setWidget(2, QFormLayout::FieldRole, authorLineEdit);

        volumeLabel = new QLabel(formLayoutWidget);
        volumeLabel->setObjectName(QString::fromUtf8("volumeLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, volumeLabel);

        volumeLineEdit = new QLineEdit(formLayoutWidget);
        volumeLineEdit->setObjectName(QString::fromUtf8("volumeLineEdit"));

        formLayout->setWidget(3, QFormLayout::FieldRole, volumeLineEdit);

        cityLabel = new QLabel(formLayoutWidget);
        cityLabel->setObjectName(QString::fromUtf8("cityLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, cityLabel);

        cityLineEdit = new QLineEdit(formLayoutWidget);
        cityLineEdit->setObjectName(QString::fromUtf8("cityLineEdit"));

        formLayout->setWidget(4, QFormLayout::FieldRole, cityLineEdit);

        publisherLabel = new QLabel(formLayoutWidget);
        publisherLabel->setObjectName(QString::fromUtf8("publisherLabel"));

        formLayout->setWidget(5, QFormLayout::LabelRole, publisherLabel);

        publisherLineEdit = new QLineEdit(formLayoutWidget);
        publisherLineEdit->setObjectName(QString::fromUtf8("publisherLineEdit"));

        formLayout->setWidget(5, QFormLayout::FieldRole, publisherLineEdit);

        yearLabel = new QLabel(formLayoutWidget);
        yearLabel->setObjectName(QString::fromUtf8("yearLabel"));

        formLayout->setWidget(6, QFormLayout::LabelRole, yearLabel);

        yearLineEdit = new QLineEdit(formLayoutWidget);
        yearLineEdit->setObjectName(QString::fromUtf8("yearLineEdit"));

        formLayout->setWidget(6, QFormLayout::FieldRole, yearLineEdit);


        retranslateUi(addBookDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), addBookDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), addBookDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(addBookDialog);
    } // setupUi

    void retranslateUi(QDialog *addBookDialog)
    {
        addBookDialog->setWindowTitle(QApplication::translate("addBookDialog", "Dialog", 0));
        iSBNLabel->setText(QApplication::translate("addBookDialog", "ISBN", 0));
        titleLabel->setText(QApplication::translate("addBookDialog", "Title", 0));
        authorLabel->setText(QApplication::translate("addBookDialog", "Author", 0));
        volumeLabel->setText(QApplication::translate("addBookDialog", "Volume", 0));
        cityLabel->setText(QApplication::translate("addBookDialog", "City", 0));
        publisherLabel->setText(QApplication::translate("addBookDialog", "Publisher", 0));
        yearLabel->setText(QApplication::translate("addBookDialog", "Year", 0));
    } // retranslateUi

};

namespace Ui {
    class addBookDialog: public Ui_addBookDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDBOOKDIALOG_H
