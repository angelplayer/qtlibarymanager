/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QVariant>
#include <qt5/QtWidgets/QAction>
#include <qt5/QtWidgets/QApplication>
#include <qt5/QtWidgets/QButtonGroup>
#include <qt5/QtWidgets/QComboBox>
#include <qt5/QtWidgets/QHeaderView>
#include <qt5/QtWidgets/QLineEdit>
#include <qt5/QtWidgets/QMainWindow>
#include <qt5/QtWidgets/QMenu>
#include <qt5/QtWidgets/QMenuBar>
#include <qt5/QtWidgets/QPushButton>
#include <qt5/QtWidgets/QTabWidget>
#include <qt5/QtWidgets/QTreeWidget>
#include <qt5/QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionTest;
    QAction *actionOpenDb;
    QAction *actionAbout;
    QAction *actionSaveDb;
    QAction *actionExit;
    QWidget *centralWidget;
    QTabWidget *Uebersicht;
    QWidget *uebersicht;
    QPushButton *addIsbn;
    QLineEdit *isbn;
    QTreeWidget *bookList;
    QWidget *testets;
    QComboBox *comboBox;
    QLineEdit *lineEdit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuInfo;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1061, 674);
        actionTest = new QAction(MainWindow);
        actionTest->setObjectName(QString::fromUtf8("actionTest"));
        actionOpenDb = new QAction(MainWindow);
        actionOpenDb->setObjectName(QString::fromUtf8("actionOpenDb"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionSaveDb = new QAction(MainWindow);
        actionSaveDb->setObjectName(QString::fromUtf8("actionSaveDb"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        Uebersicht = new QTabWidget(centralWidget);
        Uebersicht->setObjectName(QString::fromUtf8("Uebersicht"));
        Uebersicht->setGeometry(QRect(10, 20, 1041, 601));
        sizePolicy.setHeightForWidth(Uebersicht->sizePolicy().hasHeightForWidth());
        Uebersicht->setSizePolicy(sizePolicy);
        uebersicht = new QWidget();
        uebersicht->setObjectName(QString::fromUtf8("uebersicht"));
        addIsbn = new QPushButton(uebersicht);
        addIsbn->setObjectName(QString::fromUtf8("addIsbn"));
        addIsbn->setEnabled(true);
        addIsbn->setGeometry(QRect(300, 530, 93, 27));
        isbn = new QLineEdit(uebersicht);
        isbn->setObjectName(QString::fromUtf8("isbn"));
        isbn->setGeometry(QRect(122, 530, 171, 27));
        isbn->setFrame(true);
        bookList = new QTreeWidget(uebersicht);
        bookList->setObjectName(QString::fromUtf8("bookList"));
        bookList->setGeometry(QRect(10, 20, 1021, 501));
        bookList->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        bookList->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        bookList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        bookList->setAlternatingRowColors(true);
        bookList->setSortingEnabled(true);
        Uebersicht->addTab(uebersicht, QString());
        testets = new QWidget();
        testets->setObjectName(QString::fromUtf8("testets"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(testets->sizePolicy().hasHeightForWidth());
        testets->setSizePolicy(sizePolicy1);
        Uebersicht->addTab(testets, QString());
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(410, 10, 101, 32));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(520, 10, 191, 32));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1061, 30));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuInfo = new QMenu(menuBar);
        menuInfo->setObjectName(QString::fromUtf8("menuInfo"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuInfo->menuAction());
        menuFile->addAction(actionOpenDb);
        menuFile->addAction(actionSaveDb);
        menuFile->addAction(actionExit);
        menuInfo->addAction(actionAbout);

        retranslateUi(MainWindow);
        QObject::connect(actionExit, SIGNAL(triggered()), MainWindow, SLOT(close()));

        Uebersicht->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "B\303\274cherverwaltung 0.1 Alpha", 0 ));
        actionTest->setText(QApplication::translate("MainWindow", "test", 0));
        actionOpenDb->setText(QApplication::translate("MainWindow", "&open", 0));
        actionAbout->setText(QApplication::translate("MainWindow", "&about", 0));
        actionSaveDb->setText(QApplication::translate("MainWindow", "&save", 0));
        actionExit->setText(QApplication::translate("MainWindow", "&exit", 0));
        addIsbn->setText(QApplication::translate("MainWindow", "add ISBN", 0));
        isbn->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem = bookList->headerItem();
        ___qtreewidgetitem->setText(7, QApplication::translate("MainWindow", "Jahr", 0));
        ___qtreewidgetitem->setText(6, QApplication::translate("MainWindow", "Land", 0));
        ___qtreewidgetitem->setText(5, QApplication::translate("MainWindow", "Verleger", 0));
        ___qtreewidgetitem->setText(4, QApplication::translate("MainWindow", "Auflage", 0));
        ___qtreewidgetitem->setText(3, QApplication::translate("MainWindow", "Autor", 0));
        ___qtreewidgetitem->setText(2, QApplication::translate("MainWindow", "Titel", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("MainWindow", "ISBN", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("MainWindow", "index", 0));
        Uebersicht->setTabText(Uebersicht->indexOf(uebersicht), QApplication::translate("MainWindow", "Inventar", 0));
#ifndef QT_NO_ACCESSIBILITY
        testets->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        Uebersicht->setTabText(Uebersicht->indexOf(testets), QApplication::translate("MainWindow", "Verleih", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Titel", 0)
        );
        menuFile->setTitle(QApplication::translate("MainWindow", "fi&le", 0));
        menuInfo->setTitle(QApplication::translate("MainWindow", "&info", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
