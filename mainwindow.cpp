#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addbookdialog.h"
#include <qdebug.h>
#include "worldcat.h"
#include <qobject.h>
#include "isbn.h"
#include <QtWidgets/QFileDialog>
#include <QtSql>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionSaveDb,SIGNAL(triggered()),this,SLOT(saveFile()));
    connect(ui->actionOpenDb,SIGNAL(triggered()),this,SLOT(openFile()));

    QTreeWidgetItem *item = new QTreeWidgetItem(0);
    item->setText(0, QString("000000000000"));
    item->setText(1, QString("title"));
    item->setText(2, QString("author"));
    item->setText(3, QString("volume"));
    item->setText(4, QString("volume"));
    item->setText(5, QString("volume"));
    item->setText(6, QString("volume"));
    item->setText(7, QString("volume"));

    QTreeWidgetItem *item2 = new QTreeWidgetItem(0);
    item2->setText(0, QString("000000000000"));
    item2->setText(1, QString("title"));
    item2->setText(2, QString("author"));
    item2->setText(3, QString("volume"));
    item2->setText(4, QString("volume"));
    item2->setText(5, QString("volume"));
    item2->setText(6, QString("volume"));
    item2->setText(7, QString("volume"));

    QTreeWidgetItem *item3 = new QTreeWidgetItem(0);
    item3->setText(0, QString("000000000000"));
    item3->setText(1, QString("title"));
    item3->setText(2, QString("author"));
    item3->setText(3, QString("volume"));
    item3->setText(4, QString("volume"));
    item3->setText(5, QString("volume"));
    item3->setText(6, QString("volume"));
    item3->setText(7, QString("volume"));

    item->addChild(item2);
    ui->bookList->addTopLevelItem(item);
    QList<QTreeWidgetItem*> items = ui->bookList->findItems(QString("title"),Qt::MatchExactly,1);
    for(int i=0;i<items.length() ;i++){
        items.at(i)->insertChild(0,item3);
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addIsbn_clicked()
{
    Isbn isbn(ui->isbn->text());
    if(bookTemp!=NULL){
        delete bookTemp;
    }
    bookTemp = new book();
    if(worldcatLookUp!=NULL){
        delete worldcatLookUp;
    }
    worldcatLookUp = new Worldcat(bookTemp,isbn.getIsbn());
    connect(worldcatLookUp,SIGNAL(worldcatFinished()),this,SLOT(callDialog()));
}
void MainWindow::callDialog(){
    if(bookTemp->getIsbn().length()){
        ui->isbn->setStyleSheet("QLineEdit{background:none;}");
        dialog1 = new addBookDialog;
        //connect(this,SIGNAL(addIsbn(Worldcat*)),dialog1,SLOT(setValues(Worldcat*)));
        dialog1->setValues(bookTemp);
        connect(dialog1,SIGNAL(finished()),this,SLOT(addEntry()));
        //emit addIsbn(worldcatLookUp);
        dialog1->show();
    }else{
        ui->isbn->setStyleSheet("QLineEdit{background:red;}");
    }
}
void MainWindow::addEntry(){
    bookList.add(*bookTemp);
    rebuildList();
}

void MainWindow::removeEntry(int id){
    bookList.removeBook(id);
    rebuildList();
}

void MainWindow::rebuildList(){
    ui->bookList->clear();
    for(int i=0;i<bookList.length();i++){
        QTreeWidgetItem *itemNew = new QTreeWidgetItem(0);
        itemNew->setText(1,bookList.at(i)->getIsbn());
        itemNew->setText(2,bookList.at(i)->getTitle());
        itemNew->setText(3,bookList.at(i)->getAuthor());
        itemNew->setText(4,bookList.at(i)->getEdition());
        itemNew->setText(5,bookList.at(i)->getPublisher());
        itemNew->setText(6,bookList.at(i)->getCity());
        itemNew->setText(7,bookList.at(i)->getYear());
        ui->bookList->addTopLevelItem(itemNew);
    }
}

void MainWindow::openFile(){
    QFileDialog fileDialog;

    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setNameFilter("json Database(*.json)");
    fileDialog.setDefaultSuffix(".json");

    fileDialog.exec();
    QFile file(fileDialog.selectedFiles()[0]);
    file.open(QFile::ReadOnly|QFile::Text);
    QByteArray file1 = file.readAll();
    file.close();
    QJsonDocument json = QJsonDocument::fromJson(file1);
    QList<QTreeWidgetItem*> items;

    for(int i=1; i<json.array().size();i++){
        QTreeWidgetItem *item = new QTreeWidgetItem;
        QJsonValue value1 =json.array().at(i);
        QJsonArray array2 = value1.toArray();
        for(int j=0;j<array2.size();j++){
            QString string1 = array2.at(j).toString();
            item->setText(j,string1);
            qDebug()<<"file: "<<string1;
        }
        items.append(item);
    }
    ui->bookList->clear();
    ui->bookList->addTopLevelItems(items);
}

void MainWindow::saveFile(){
    QJsonObject jsonObj;
    QJsonArray jsonArr1;
    for(int i=0; i<ui->bookList->topLevelItemCount();i++){
        QJsonArray jsonArr;
        qDebug()<<ui->bookList->topLevelItem(i)->childCount();
        for(int j=0; j<ui->bookList->topLevelItem(i)->columnCount();j++){
            jsonArr.append(ui->bookList->topLevelItem(i)->data(j,0).toString());
        }
        jsonArr1.append(jsonArr);
    }
    QJsonDocument json(jsonArr1);
    QFileDialog fileDialog;
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setNameFilter("json Database(*.json)");
    fileDialog.setDefaultSuffix(".json");
    fileDialog.exec();
    QFile file(fileDialog.selectedFiles()[0]);
    file.open(QFile::WriteOnly);
    file.write(json.toJson());
    file.close();

}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    qDebug()<<arg1;
    /*QList<QTreeWidgetItem*> searchList = ui->bookList->findItems(arg1,Qt::MatchContains|Qt::MatchRecursive,2);
   // ui->bookList->clear();
    for(int i=0;i<searchList.size();i++){
        qDebug()<<"result:"<<searchList.at(i)->data(2,0);
    }
    ui->bookList->addTopLevelItems(searchList);*/
    bookList.setFilter(arg1,2);
    rebuildList();
}
