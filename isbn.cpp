#include "isbn.h"
#include <QtCore>
#include <QObject>

Isbn::Isbn()
{
}

Isbn::~Isbn(){
}

Isbn::Isbn(QString isbn){
    setIsbn(isbn);
}

void Isbn::setIsbn(QString isbn){
    isbn = cleanIsbn(isbn);
    if(validateIsbn(isbn)){
        if(isbn.length()==13){
            this->isbn_nr = isbn;
        }else{
            this->isbn_nr = from10to13(isbn);
        }
    }
}

QString Isbn::getIsbn(){
    return this->isbn_nr;
}

QString Isbn::cleanIsbn(QString isbn){
    isbn.replace("-","");
    isbn.replace(" ","");
    isbn.toUpper();
    return isbn;
}

int Isbn::validateIsbn(QString isbn){
    QChar checksumChar = isbn.at(isbn.length()-1);
    if(isbn.length()==13){
        if(calcIsbn13Checksum(isbn)==checksumChar){
            return true;
        }else{
            return false;
        }
    }else if(isbn.length()==10){
        if(calcIsbn10Checksum(isbn)==checksumChar){
            return true;
        }else{
            return false;
        }
    }
    return false;
}

QChar Isbn::calcIsbn10Checksum(QString isbn){
    isbn = isbn.mid(0,9);
    int sum = 0;
    for(int i=0;i<isbn.length();i++){
        sum += (isbn.at(i)).digitValue() *(10-i);
    }
    sum =(11-(sum%11))%11;
    if(sum == 10){
        return QChar('X');
    }else{
        return QString::number(sum).at(0);
    }
}

QChar Isbn::calcIsbn13Checksum(QString isbn){
    isbn = isbn.mid(0,12);
    int sum = 0;
    for(int i=0;i<isbn.length();i++){
        if(i%2){
            sum += (isbn.at(i)).digitValue()*3;
        } else{
            sum += (isbn.at(i)).digitValue();
        }
    }

    sum = 10-(sum%10);
    if(sum == 10){
        sum = 0;
    }

    return QString::number(sum).at(0);
}

QString Isbn::from10to13(QString isbn){
    isbn.prepend(QString("978"));
    isbn = isbn.mid(0,12);
    isbn.append(calcIsbn13Checksum(isbn));
    return isbn;
}
