#ifndef BOOK_H
#define BOOK_H
#include <QObject>
#include <QSqlRecord>
#include "inventory_object.h"

class book : inventory_object
{

public:
    book();
    book(QString isbn, QString author, QString title, QString edition, QString publisher, QString year, QString city, QString language);
    QString getIsbn() const;
    QString getAuthor() const;
    QString getTitle() const;
    QString getEdition() const;
    QString getPublisher() const;
    QString getYear() const;
    QString getCity() const;
    QString getLanguage() const;

    void setIsbn(QString isbn);
    void setAuthor(QString author);
    void setTitle(QString title);
    void setEdition(QString edition);
    void setPublisher(QString publisher);
    void setYear(QString year);
    void setCity(QString city);
    void setLanguage(QString language);
    static book fromRow(QSqlRecord);
private:
    int id;
    QString isbn;
    QString author;
    QString title;
    QString edition;
    QString publisher;
    QString year;
    QString city;
    QString language;

signals:

public slots:

};

#endif // BOOK_H
