#ifndef ISBN_H
#define ISBN_H
#include <QtCore>
#include <QObject>

class Isbn : public QObject
{
   Q_OBJECT

public:
    Isbn();
    Isbn(QString);
    QString getIsbn();
    void setIsbn(QString);
    ~Isbn();
private:
    QString isbn_nr;
    QString cleanIsbn(QString);
    int validateIsbn(QString);
    QChar calcIsbn10Checksum(QString);
    QChar calcIsbn13Checksum(QString);
    QString from10to13(QString);
};

#endif // ISBN_H
